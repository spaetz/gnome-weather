# Croatian translation for gnome-weather.
# Copyright (C) 2017 gnome-weather's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-weather package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-weather master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-weather/issues\n"
"POT-Creation-Date: 2021-02-12 20:57+0000\n"
"PO-Revision-Date: 2021-02-26 10:19+0100\n"
"Last-Translator: gogo <trebelnik2@gmail.com>\n"
"Language-Team: Croatian <hr@li.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.3\n"

#: data/org.gnome.Weather.appdata.xml.in.in:5
#: data/org.gnome.Weather.desktop.in.in:4 data/window.ui:89 src/app/main.js:64
#: src/app/window.js:203 src/service/main.js:48
msgid "Weather"
msgstr "Vrijeme"

#: data/org.gnome.Weather.appdata.xml.in.in:6
#: data/org.gnome.Weather.desktop.in.in:5
msgid "Show weather conditions and forecast"
msgstr "Pogledajte vašu lokalnu vremensku prognozu"

#: data/org.gnome.Weather.appdata.xml.in.in:16
msgid ""
"A small application that allows you to monitor the current weather "
"conditions for your city, or anywhere in the world."
msgstr ""
"Mala aplikacija koja vam omogućuje praćenje trenutne vremenske prognoze u "
"vašem gradu, ili bilo gdje na svijetu."

#: data/org.gnome.Weather.appdata.xml.in.in:20
msgid ""
"It provides access to detailed forecasts, up to 7 days, with hourly details "
"for the current and next day, using various internet services."
msgstr ""
"Omogućuje vam pristup opširnoj prognozi vremena, do 7dana, sa satnim "
"pojedinostima za trenutni i sljedeći dan, koristeći razne internetske usluge."

#: data/org.gnome.Weather.appdata.xml.in.in:24
msgid ""
"It also optionally integrates with the GNOME Shell, allowing you to see the "
"current conditions of the most recently searched cities by just typing the "
"name in the Activities Overview."
msgstr ""
"Isto tako se po izboru integrira s GNOME ljuskom, omogućujući vam pregled "
"trenutne vremenske prognoze najčešće traženih gradova, jednostavno samo "
"upisivanjem naziva grada u Aktivnosti pregledu."

#: data/org.gnome.Weather.appdata.xml.in.in:118
msgid "The GNOME Project"
msgstr "GNOME projekt"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Weather.desktop.in.in:13
msgid "Weather;Forecast;"
msgstr "Vrijeme;Prognoza;"

#: data/org.gnome.Weather.desktop.in.in:14
msgid "Allows weather information to be displayed for your location."
msgstr "Omogućuje prikaz vremenskih informacija za vašu lokaciju."

#: data/org.gnome.Weather.gschema.xml:6
msgid "Configured cities to show weather for"
msgstr "Podešeni gradovi za koje će se prikazati prognoza"

#: data/org.gnome.Weather.gschema.xml:7
msgid ""
"The locations shown in the world view of gnome-weather. Each value is a "
"GVariant returned by gweather_location_serialize()."
msgstr ""
"Lokacija prikazana u pregledu svijeta u gnome-weather. Svaka vrijednost je "
"GVariant vraćen od gweather_location_serialize()."

#: data/org.gnome.Weather.gschema.xml:14
msgid "Automatic location"
msgstr "Automatska lokacija"

#: data/org.gnome.Weather.gschema.xml:15
msgid ""
"The automatic location is the value of automatic-location switch which "
"decides whether to fetch current location or not."
msgstr ""
"Automatska lokacija je vrijednost automatic-location preklopnika koji "
"odlučuje treba li preuzeti trenutnu lokaciju ili ne."

#: data/city.ui:9
msgid "City view"
msgstr "Prikaz grada"

#: data/city.ui:32
msgid "Loading…"
msgstr "Učitavanje…"

#: data/day-entry.ui:26
msgid "Night"
msgstr "Noć"

#: data/day-entry.ui:41
msgid "Morning"
msgstr "Jutro"

#: data/day-entry.ui:56
msgid "Afternoon"
msgstr "Poslijepodne"

#: data/day-entry.ui:71
msgid "Evening"
msgstr "Večer"

#: data/places-popover.ui:46
msgid "Automatic Location"
msgstr "Automatska lokacija"

#: data/places-popover.ui:85
msgid "Locating…"
msgstr "Lociranje…"

#: data/places-popover.ui:142
msgid "Search for a city"
msgstr "Potraži grad"

#: data/places-popover.ui:174
msgid "Viewed Recently"
msgstr "Nedavno pogledano"

#: data/primary-menu.ui:4 data/window.ui:7
msgid "_Temperature Unit"
msgstr "_Jedinica temperature"

#: data/primary-menu.ui:6 data/window.ui:9
msgid "_Celsius"
msgstr "_Celzijus"

#: data/primary-menu.ui:11 data/window.ui:14
msgid "_Fahrenheit"
msgstr "_Farenhajt"

#: data/primary-menu.ui:19 data/window.ui:22
msgid "_About Weather"
msgstr "_O Vremenu"

#: data/weather-widget.ui:71
msgid "Places"
msgstr "Lokacije"

#: data/weather-widget.ui:147
msgid "Current conditions"
msgstr "Trenutno stanje"

#: data/weather-widget.ui:179
msgid "Hourly"
msgstr "Satno"

#: data/weather-widget.ui:200
msgid "Daily"
msgstr "Dnevno"

#: data/window.ui:51
msgid "Refresh"
msgstr "Osvježi"

#: data/window.ui:77
msgid "Select Location"
msgstr "Odaberi lokaciju"

#: data/window.ui:126
msgid "Welcome to Weather!"
msgstr "Dobrodošli u vrijeme!"

#: data/window.ui:127
msgid "To get started, select a location."
msgstr "Kako bi započeli, odaberite lokaciju."

#: src/app/city.js:211
#, javascript-format
msgid "Feels like %.0f°"
msgstr "Kao da je %.0f°"

#: src/app/city.js:243
msgid "Updated just now."
msgstr "Nadopunjeno upravo sada."

#: src/app/city.js:248
#, javascript-format
msgid "Updated %d minute ago."
msgid_plural "Updated %d minutes ago."
msgstr[0] "Nadopunjeno prije %d minute."
msgstr[1] "Nadopunjeno prije %d minute."
msgstr[2] "Nadopunjeno prije %d minuta."

#: src/app/city.js:254
#, javascript-format
msgid "Updated %d hour ago."
msgid_plural "Updated %d hours ago."
msgstr[0] "Nadopunjeno prije %d sata."
msgstr[1] "Nadopunjeno prije %d sata."
msgstr[2] "Nadopunjeno prije %d sati."

#: src/app/city.js:260
#, javascript-format
msgid "Updated %d day ago."
msgid_plural "Updated %d days ago."
msgstr[0] "Nadopunjeno prije %d dana."
msgstr[1] "Nadopunjeno prije %d dana."
msgstr[2] "Nadopunjeno prije %d dana."

#: src/app/city.js:266
#, javascript-format
msgid "Updated %d week ago."
msgid_plural "Updated %d weeks ago."
msgstr[0] "Nadopunjeno prije %d tjedna."
msgstr[1] "Nadopunjeno prije %d tjedna."
msgstr[2] "Nadopunjeno prije %d tjedana."

#: src/app/city.js:271
#, javascript-format
msgid "Updated %d month ago."
msgid_plural "Updated %d months ago."
msgstr[0] "Nadopunjeno prije %d mjeseca."
msgstr[1] "Nadopunjeno prije %d mjeseca."
msgstr[2] "Nadopunjeno prije %d mjeseci."

#: src/app/dailyForecast.js:38
msgid "Daily Forecast"
msgstr "Dnevna prognoza"

#: src/app/dailyForecast.js:95 src/app/hourlyForecast.js:91
msgid "Forecast not available"
msgstr "Prognoza nije dostupna"

#. Translators: this is the time format for day and month name according to the current locale
#: src/app/dailyForecast.js:181
msgid "%b %e"
msgstr "%e %b"

#: src/app/hourlyForecast.js:42
msgid "Hourly Forecast"
msgstr "Satna prognoza"

#. Translators: this is a time format without date used for AM/PM
#: src/app/hourlyForecast.js:109
msgid "%l∶%M %p"
msgstr "%H:%M"

#: src/app/window.js:202
msgid "translator-credits"
msgstr "gogo https://launchpad.net/~trebelnik-stefina"

#: src/app/window.js:204
msgid "A weather application"
msgstr "Aplikacija vremenske prognoze"

#: src/app/world.js:38
msgid "World view"
msgstr "Prikaz svijeta"

#. TRANSLATORS: this is the temperature string, minimum and maximum.
#. The two values are already formatted, so it would be something like
#. "7 °C / 19 °C"
#: src/misc/util.js:140
#, javascript-format
msgid "%s / %s"
msgstr "%s / %s"

#. TRANSLATORS: this is the description shown in the overview search
#. It's the current weather conditions followed by the temperature,
#. like "Clear sky, 14 °C"
#: src/service/searchProvider.js:182
#, javascript-format
msgid "%s, %s"
msgstr "%s, %s"

#~ msgid "Search for a location"
#~ msgstr "Potraži lokaciju"

#~ msgid "To see weather information, enter the name of a city."
#~ msgstr "Kako bi vidjeli vremenske informacije, upišite naziv grada."

#~ msgid "%e %b"
#~ msgstr "%-e %b"

#~ msgid "Today"
#~ msgstr "Danas"

#~ msgid "Tomorrow"
#~ msgstr "Sutra"

#~ msgid "Forecast"
#~ msgstr "Prognoza"

#~ msgid "%R"
#~ msgstr "%R"

#~ msgid "%A"
#~ msgstr "%A"

#~ msgid "@APP_ID@"
#~ msgstr "@APP_ID@"

#~ msgid "_New"
#~ msgstr "_Nova"

#~ msgid "About"
#~ msgstr "O programu"

#~ msgid "Quit"
#~ msgstr "Zatvori"
